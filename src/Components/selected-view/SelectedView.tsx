import React, { Component } from 'react';
import { broadcastService } from '../../Helpers/BroadcastService';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import './SelectedView.scss';
import { saveOrder } from '../../Connectors/CommonService';
import Swal from 'sweetalert2';

// Reorder the list items
const reorder = (list: Iterable<any>, startIndex: number, endIndex: number) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

class SelectedView extends Component<{}, { selectedImages: Array<any> }> {
  constructor(props: any) {
    super(props);
    this.state = {
      selectedImages: [],
    };
    this.onDragEnd = this.onDragEnd.bind(this);
  }
/**
 * onDragEnd handles the drag action from react-beautiful-dnd
 * Re-arrange the image array with the user changes and update the state
 */
  onDragEnd(result: any) {
    // if item dropped outside the list
    if (!result.destination) {
      return;
    }
    const selects = reorder(
      this.state.selectedImages,
      result.source.index,
      result.destination.index
    );
    selects.map((image: any, index: number) => {
      image.index = index;
      return image;
    });
    this.setState({
      selectedImages: selects,
    });
  }

  componentDidMount() {
    broadcastService.on('selectedImageArray').subscribe((res: any) => {
      const { images } = res;
      this.setState({ selectedImages: images });
    });
  }

  /**
   * @description request body should contain sortedList value @type Array 
   * this function saves the user input
   */
  saveOrder() {
    saveOrder({
      sortedList: this.state.selectedImages.map((image: any, index: number) => {
        return { id: image.id, index };
      }),
      albumId:'60a7d9f796b2353fa1c1b1b5'
    }).subscribe((res: any) => {
      Swal.fire('successful', 'saved your selection!!', 'success');
    });
  }

  render() {
    return (
      <div className='selected-view'>
        <h1>Your best 9 photos</h1>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Droppable direction='horizontal' droppableId='droppable'>
            {(provided, snapshot) => (
              <div
                className='image-container'
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                {this.state.selectedImages.map((item: any, index: number) => (
                  <Draggable
                    key={index}
                    draggableId={index.toString()}
                    index={index}
                  >
                    {(provided, snapshot) => (
                      <div
                        className='image-view'
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <img
                          src={item.picture}
                          alt='...'
                          height='100'
                          width='100'
                        />
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
        <div className='btn-container'>
          <button
            type='button'
            className='btn btn-success'
            onClick={(e: any) => {
              this.saveOrder();
            }}
          >
            Save
          </button>
        </div>
      </div>
    );
  }
}

export default SelectedView;
