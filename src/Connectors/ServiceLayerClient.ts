import Axios from 'axios-observable';
function getPublicHeaders() {
  return {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
}
function publicPost(url: string, payload: any, headers = { headers: getPublicHeaders() }) {
  try {
    return Axios.post(url,payload, headers);
  } catch (error) {
    console.log(error);
    throw error;
  }
}

function publicPatch(url: string, payload: any, headers = { headers: getPublicHeaders() }) {
  try {
    return Axios.patch(url,payload, headers);
  } catch (error) {
    console.log(error);
    throw error;
  }
}
function publicGet(url: string, params?: any) {
  try {
    return Axios.get(url, { headers: getPublicHeaders(), params });
  } catch (error) {
    console.log(error);
    throw error;
  }
}
export {
  publicGet, publicPost, publicPatch
}
