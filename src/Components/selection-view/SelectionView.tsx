import React, { Component } from 'react';
import Swal from 'sweetalert2';
import { getImages, getOrder } from '../../Connectors/CommonService';
import { broadcastService } from '../../Helpers/BroadcastService';
import * as _ from 'lodash';
import './SelectionView.scss';
class SelectionView extends Component<
  {},
  { images: any; selectedImages: any; maxSelection: number }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      images: [],
      selectedImages: [],
      maxSelection: 9,
    };
  }
  componentDidMount() {
    getOrder().subscribe((res: any) => {
      console.log(res);

      const {
        data: { data: responseData = null },
      } = res;
      if (!_.isNull(responseData)) {
        this.loadAllImages(responseData.sortedList);
      } else {
        this.loadAllImages([]);
      }
    });
  }
  /**
   * load all the images from give test end-point
   * Get the previous order
   */
  loadAllImages(sortedList: Array<any>) {
    const sortedListImageIdMap = sortedList.map((image: any) => image.id);
    const selectedImageMap = _.keyBy(sortedList, (image) => image.id);
    getImages().subscribe((res: any) => {
      const {
        data: { entries },
      } = res;
      entries.map((imagedata: any) => {
        if (
          sortedListImageIdMap.some((imageId: any) => imageId === imagedata.id)
        ) {
          imagedata.isActive = true;
          imagedata.index = selectedImageMap[imagedata.id].index;
          this.state.selectedImages.push(imagedata);
        } else {
          imagedata.isActive = false;
        }
        return imagedata;
      });

      this.setState({ images: entries });
      this.updateSelectedView();
    });
  }
  /* Validate the index and reorder the array base on index value
   * Broadcast the updated image array
   */
  updateSelectedView() {
    this.setState(
      {
        selectedImages: this.state.selectedImages.filter(
          (image: any) => image.isActive
        ),
      },
      () => {
        broadcastService.broadcast('selectedImageArray', {
          images: _.orderBy(
            this.state.selectedImages,
            (image: any) => image.index
          ),
        });
      }
    );
  }
  /**
   * handles the image selection and restriction of the selection
   * @param event , change event of the check input
   * @param selectedImage , data part of the input change
   */
  changeSelection(event: any, selectedImage: any) {
    const { id: inputImageId } = selectedImage;
    const {
      currentTarget: { checked },
    } = event;

    if (this.state.selectedImages.length < this.state.maxSelection && checked) {
      this.state.images.map((image: any) => {
        const { id } = image;
        if (id === inputImageId) {
          image.isActive = true;
          this.state.selectedImages.push(image);
        }
        return image;
      });
    } else if (!checked) {
      this.state.images.map((image: any) => {
        const { id } = image;
        if (id === inputImageId) {
          image.isActive = false;
          this.state.selectedImages.push(image);
        }
        return image;
      });
    } else {
      Swal.fire(
        'Maximum image count exceeded',
        `you can only select up to ${this.state.maxSelection} images`,
        'warning'
      );
      event.currentTarget.checked = false;
    }

    this.updateSelectedView();
  }

  render() {
    return (
      <div className='selection-view'>
        {this.state.images.map((image: any, key: number) => (
          <div key={key}>
            <img src={image.picture} alt='...' height='200' width='200' />
            <div className='switch-container'>
              <label className='switch'>
                <input
                  type='checkbox'
                  name=''
                  id=''
                  className='form-check-input'
                  onChange={(e: any) => {
                    this.changeSelection(e, image);
                  }}
                  checked={image.isActive}
                />
                <span className='slider round'></span>
              </label>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default SelectionView;
