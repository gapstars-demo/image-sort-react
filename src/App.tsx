import React, { Component, lazy, Suspense } from 'react';
import './App.scss';
const SelectionView = lazy(
  () => import('./Components/selection-view/SelectionView')
);
const SelectedView = lazy(
  () => import('./Components/selected-view/SelectedView')
);
class App extends Component {
  render() {
    return (
      <div className='container wrap'>
        <div className='row '>
          <Suspense fallback={null}>
            <SelectedView />
          </Suspense>
        </div>
        <div className='row'>
          <Suspense fallback={null}>
            <SelectionView />
          </Suspense>
        </div>
      </div>
    );
  }
}

export default App;
