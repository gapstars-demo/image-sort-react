# GapstarsPhotoSortApp

## Configurations
- Find the Config folder /src/Config
- change the required vales
- BASE_URL is configured to listen to localhot , 4060 port which is to align with api docker image port, can be changed if needed

## Warning!
- Use .env file for string config values if its serving for several environments, in this example, it uses a constant file only for dev purpose.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## How it works. 
 - Once the project is built or served , navigate to http://localhost:3000
 - Then select/ toggle on any image from the bottom container
 - Drag and sort the order of the selected images
 - Save changes using the button

## description

Once you select and sort images and make changes saved. you can refresh the page or revisit the site later.
This project saves your previous changes with the choice you made for the images
