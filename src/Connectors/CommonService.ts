import { publicGet, publicPatch, publicPost } from "./ServiceLayerClient";
import { BASE_URL, IMAGE_URL } from '../Config/Constant';
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

function getImages(): Observable<any> {
    return publicGet(IMAGE_URL).pipe(map((res: any) => res));

}
function getOrder(options?: any): Observable<any> {
    return publicGet(`${BASE_URL}/album/60a7d9f796b2353fa1c1b1b5`, { ...options }).pipe(map((res: any) => res));
}
function saveOrder(payload: any): Observable<any> {
    return publicPatch(`${BASE_URL}/album`, {
        ...payload
    }).pipe(map((res: any) => res));
}

export {
    getImages, getOrder, saveOrder
}
