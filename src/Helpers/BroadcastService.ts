import { Subject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

const subject = new Subject();

export const broadcastService = {
    broadcast: (key: string, data: any) => subject.next({ key, data }),
    on: (key: string) => subject.asObservable().pipe(filter((event: any) => event.key === key)).pipe(map((event: any) => event.data))
};
